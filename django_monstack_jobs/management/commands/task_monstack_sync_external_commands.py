from django.core.management.base import BaseCommand

from django_monstack_jobs.django_monstack_jobs.tasks import (
    monstack_sync_external_commands,
)


class Command(BaseCommand):
    help = "monstack_sync_external_commands"

    def handle(self, *args, **options):
        # New Installation Date
        monstack_sync_external_commands()
