from celery import shared_task
from django.apps import apps
from django.template import engines

from django_cdstack_deploy.django_cdstack_deploy.func import get_host_vars
from django_cdstack_models.django_cdstack_models.models import *
from django_monstack_icinga2.django_monstack_icinga2.director_api import *
from django_monstack_icinga2.django_monstack_icinga2.models_icinga2_director import (
    IcingaHost,
)
from django_monstack_notifier_api.django_monstack_notifier_api.models import (
    MonNotifierQueue,
)
from django_monstack_opennms.django_monstack_opennms.opennms_api import (
    opennms_get_or_create_host,
)
from django_monstack_wazuh.django_monstack_wazuh.wazuh_api import (
    wazuh_get_or_create_host,
)
from django_simple_notifier.django_simple_notifier.models import SnotifierTemplates
from django_simple_notifier.django_simple_notifier.plugin_zammad import (
    send as send_ticket,
)


@shared_task(name="monstack_sync_hosts")
def monstack_sync_hosts():
    all_os_families = CmdbOsFamily.objects.all()
    all_os_versions = CmdbOsVersion.objects.all()

    for os_family in all_os_families:
        edit_host = dict()
        edit_host["imports"] = ("generic-vars-host",)
        get_or_create_host(
            pseudo_hostname=os_family.monitoring_id,
            merge_data=edit_host,
            skip_cmdb_id=True,
            object_type="template",
        )

    for os_version in all_os_versions:
        edit_host = dict()
        edit_host["has_agent"] = os_version.agent_based
        edit_host["imports"] = (os_version.family.monitoring_id,)
        get_or_create_host(
            pseudo_hostname=os_version.monitoring_id,
            merge_data=edit_host,
            skip_cmdb_id=True,
            object_type="template",
        )

    # TODO: Remove OS that no longer exist

    all_hosts = CmdbHost.objects.filter(mon_enable=True)
    filter_known = list()

    for host in all_hosts:
        host_vars = get_host_vars(host)

        edit_host = dict()
        edit_host["accept_config"] = True
        edit_host["master_should_connect"] = False

        if "network_iface_primary_ip" in host_vars:
            edit_host["address"] = host_vars["network_iface_primary_ip"]
        else:
            edit_host["address"] = "127.0.0.1"

        if "network_iface_primary_ip6" in host_vars:
            edit_host["address6"] = host_vars["network_iface_primary_ip6"]
        else:
            edit_host["address6"] = "::1"

        edit_host["has_agent"] = host.os.agent_based
        edit_host["vars"] = dict()
        edit_host["vars"]["check_up_down"] = host.check_up_down
        edit_host["vars"]["notify_enabled"] = host.notify_enabled

        if host.notify_enabled_override is False:
            edit_host["vars"]["notify_enabled"] = host.notify_enabled_override

        edit_host["imports"] = (host.os.monitoring_id,)

        if host.os.id == 2:
            edit_host["master_should_connect"] = False

        if len(host.dependency_parent_hosts):
            edit_host["vars"]["parents"] = list()
            for parent_host in host.dependency_parent_hosts:
                edit_host["vars"]["parents"].append(parent_host.monitoring_id)
            edit_host["vars"]["parents"].sort()

        get_or_create_host(host.monitoring_id, edit_host, host.mon_skip_cmdb_prefix)

        filter_known.append(host.monitoring_id)

    obsolete_director_hosts = (
        IcingaHost.objects.using("icinga2director")
        .filter(object_type="object")
        .exclude(object_name__in=filter_known)
    )

    for obsolete_director_host in obsolete_director_hosts:
        delete_host(obsolete_director_host.object_name)

    deploy()

    return


@shared_task(name="monstack_process_notification_queue")
def monstack_process_notification_queue():
    django_engine = engines["django"]

    all_notifications = MonNotifierQueue.objects.all().order_by("id")

    for notification in all_notifications:
        try:
            host_id = notification.data["hostname"].split("-")[0]

            if host_id[0] == "h":
                host_id = host_id[1:]

            print(host_id)

            cmdb_host = CmdbHost.objects.get(id=host_id)
        except:
            print("Host not found, dropping notification!")
            notification.delete()
            continue
        else:
            print("Send notification!")

        template_opts = dict()
        template_opts["cmdb_host"] = cmdb_host
        template_opts["notification"] = notification

        recipients = list()

        for recipient in cmdb_host.cmdbnotifieremailhost_set.all():
            recipients.append(SnotifierEmailContactZammad(email=recipient.email))

        for host_group in cmdb_host.cmdbhostgroups_set.all():
            for recipient in host_group.group_rel.cmdbnotifieremailgroup_set.all():
                recipients.append(SnotifierEmailContactZammad(email=recipient.email))

        for recipient in cmdb_host.inst_rel.cmdbnotifieremailinstance_set.all():
            recipients.append(SnotifierEmailContactZammad(email=recipient.email))

        # TODO: remove duplicates (list -> set -> list)

        subject = (
            notification.data["state"] + " - " + cmdb_host.hostname + " - Monitoring"
        )

        # Show title in subject if it exists
        if cmdb_host.title:
            subject = (
                notification.data["state"]
                + " - "
                + cmdb_host.hostname
                + " ("
                + cmdb_host.title
                + ") "
                + " - Monitoring"
            )

        # Mangle template
        django_template = str()

        if notification.data["state"] == "UP":
            django_template += (
                SnotifierTemplates.objects.get(name="monstack_template_up")
                .snotifiertemplatetranslations_set.get(lang_code="de")
                .template_text
            ) + "\r\n"
        else:
            django_template += (
                SnotifierTemplates.objects.get(name="monstack_template_down")
                .snotifiertemplatetranslations_set.get(lang_code="de")
                .template_text
            ) + "\r\n"

        django_template += (
            SnotifierTemplates.objects.get(name="email_footer")
            .snotifiertemplatetranslations_set.get(lang_code="de")
            .template_text
        ) + "\r\n"

        template = django_engine.from_string(django_template)

        text = template.render(template_opts)

        # Check if old ticketid should be reused
        time_limit = datetime.now() - timedelta(hours=25)

        if cmdb_host.mon_last_problem > time_limit:
            cmdb_host.mon_last_ticketid = send_ticket(
                notifier_list=recipients,
                subject=subject,
                text=text,
                group="Support - First-Level",
                note=None,
                ticket_id=cmdb_host.mon_last_ticketid,
                main_address=None,
            )
        else:
            cmdb_host.mon_last_ticketid = send_ticket(
                notifier_list=recipients,
                subject=subject,
                text=text,
                group="Support - First-Level",
                note=None,
                ticket_id=None,
                main_address=None,
            )

        cmdb_host.mon_last_problem = datetime.now()
        cmdb_host.save()

        notification.delete()

    return


@shared_task(name="monstack_wazuh_sync_hosts")
def monstack_wazuh_sync_hosts():
    all_hosts = CmdbHost.objects.filter(mon_enable=True)

    for host in all_hosts:
        key = wazuh_get_or_create_host(host)

        if key:
            try:
                monitoring_wazuh_agent_key = CmdbVarsHost.objects.get(
                    name="monitoring_wazuh_agent_key", host_rel=host
                )
            except CmdbVarsHost.DoesNotExist:
                monitoring_wazuh_agent_key = CmdbVarsHost(
                    name="monitoring_wazuh_agent_key", host_rel=host
                )
                monitoring_wazuh_agent_key.save(force_insert=True)

            monitoring_wazuh_agent_key.value = key
            monitoring_wazuh_agent_key.save()

    return


@shared_task(name="monstack_opennms_sync_hosts")
def monstack_opennms_sync_hosts():
    all_hosts = CmdbHost.objects.filter(mon_enable=True)

    for host in all_hosts:
        opennms_get_or_create_host(
            host.monitoring_id,
            "1abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghi64",
        )

    return


@shared_task(name="monstack_sync_external_commands")
def monstack_sync_external_commands():
    for model in apps.get_app_config("django_monstack_check_commands").models.values():
        icinga2_command_name = model().mon_core_check_name()["icinga2"]
        get_or_create_external_command(check_command=icinga2_command_name)

    deploy()

    return
